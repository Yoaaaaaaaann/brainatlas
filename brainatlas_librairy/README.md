# Brain Atlas

Brain Atlas is a package that helps you the register you brain images onto an atlas and to find specified areas in it

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install brain atlas.

```bash
pip install /path/to/dist/wheelfile.whl
```

## Usage for 2D images

```python
from brainatlas_helper import registration, mask, utils
from brainatlas_helper.area_marker import Atlas
import skimage

### Firstly we create our atlas with an svg file for the image and a json file that gather all the data on the atlas.
### The code is based of svg and json files avaible on http://help.brain-map.org/display/api/Atlas+Drawings+and+Ontologies
atlas = Atlas("svg_file.svg","json_file.json")
atlas_image_array = utils.svg_to_ndarray(atlas.atlas_image)

### Fisrt we do the registration part
# We work on gray shades images
atlas_image_gray = skimage.color.rgbtogray(atlas_image_array[...,:-1]) # we remove the last channel which is the mask channel (see below)

# Then we load our brain image width the package you want. The image has to be in gray shades
brain_image = imread("file_of_image")

## Secondly, We registered our brain onto the atlas brain
# We create the mask of our brain. 
mask_moving = mask.make_mask(brain_image)

# The mask of the atlas is directly accessible in the image of the atlas : it's the fourth channel of the image (the 3 first are the 3 colors)
mask_static = atlas_image_array[...,3]
# We can also use the mask.make_mask() function
mask_static = mask.make_mask(atlas_image_gray)

# We can now compute the registration of our images
# Several parameters are here to make the registration as perfect as possible
# You can play with them, especially the "rescale_factor" parameters.
# See help(registration.registration_2D) for more details
dipy_reg = registration.registration_2D(mask_moving, mask_static, diffeo_registration= True)

# We apply the registration
reg = registration.apply_2Dtransform(dipy_reg, brain_image, atlas_image_gray)

brain_registered = reg["final_image"]
# We can plot the result
utils.plot_images([atlas_image_array, brain_image, img_registered])

### Now we can go to area detection part
# We can see all regions that are represented on the image with :
area = atlas.get_all_atlas_areas()

# We choose a region of interest (roi)
roi = "Somatosensory areas"

# We compute the contour and the mask of this region :
contours, mask = atlas.get_contour_area(roi)

# Finally we can plot our brain image with the contour on it
fig, ax = plt.subplots(figsize=(20,15))
ax.imshow(brain_image)
for contour in contours:
    ax.plot(contour[:, 1], contour[:, 0], linewidth=2, color='k')

# And that's all !

"""
If you want to compute the registration on downscaled image to go faster, it is totally possible,
The only constraint is that the width and the height images you use have to be a multiple of 1/scale_factor.

Example : if you rescale your images by 10, the scale factor is 0.1. So the width and the height of the images have to be a multiple of 10.

The function : utils.pad_image(img, scale_factor) is here to do it for you. You don't loose any information on the images, we just add some black pixels.

Thereby, you compute the registration (the function registration_2D()) with small images and then you apply the registration to the big images (the function apply_2DTransform())

"""
```
## Usage for 3D images

```python

# It's way more simple because you don't need the masks, it works directly with the images (3D numpy array)
# There are several ways to create a numpy array from a z-stack of 2D images. I'll present you one here.
from skimage import io
from brainatlas_helper.registration import registration_3D

ic = io.imread_collection("path/to/the/zstack/*.tif")
image_moving = io.concatenate_images(ic)

ic = io.imread_collection("path/to/the/atlas-zstack/*.tif")
image_static= io.concatenate_images(ic)

# A bunch of parameters are here to make the registration you want.
# See all the parameters with help(registration_3D)
reg = registration.registration_3D(moving, static, diffeo_registration=True, disp_figure=True)

registered_image = reg["final_image"]

# And that's all !

```

## License
[MIT](https://choosealicense.com/licenses/mit/)

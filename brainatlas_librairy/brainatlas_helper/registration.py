import numpy as np

import dipy.align
from dipy.align._public import AffineMap, DiffeomorphicMap
from dipy.align.imwarp import SymmetricDiffeomorphicRegistration
from dipy.align.imaffine import AffineRegistration
from dipy.viz import regtools
from dipy.align.metrics import SSDMetric
from dipy.align.transforms import (TranslationTransform2D, RigidIsoScalingTransform2D)
from dipy.align import vector_fields as vfu
from bpdl.registration import SmoothSymmetricDiffeomorphicRegistration

from dipy.align.imaffine import (transform_centers_of_mass,
                                 AffineMap,
                                 MutualInformationMetric,
                                 AffineRegistration)
from dipy.align.transforms import (AffineTransform3D)
from dipy.align.imwarp import SymmetricDiffeomorphicRegistration
from dipy.align.metrics import CCMetric,SSDMetric

from skimage import transform
from skimage.transform import rescale

def registration_2D(moving, 
                    static, 
                    disp_figure = False, 
                    center_registration = True, 
                    diffeo_registration = False, 
                    smooth_registration= True, 
                    smooth_sigma = 10., 
                    rescale_image = False, 
                    rescale_factor= None):
    """ registration_2D(moving, static, disp_figure = False, center_registration = True, diffeo_registration = False)
    
    Compute the dipy registration on masks

    Parameters -------------------------------------------------------

    moving : the mask of the moving image - numpy 2D array (width, height)
    static : the mask of the static image - numpy 2D array (width, height)

    disp_figure : boolean : True if we want to see the figures

    center_registration : boolean : True if we want to compute a center of mass registration (sometimes it works better without)
    diffeo_registration : boolean : True if we want to compute a symmetric diffeomorphic registration (sometimes it's not useful and it takes a lot of time)
    smooth_registration : boolean : True if we want our diffeomorphic registration to be smooth
    smooth_sigma        : double  : value of the smoothness of the transformation
    rescale_image       : boolean :  True we we want to rescale the moving image at the beginning to ease the registrations
    rescale_factor      : double : Value of the rescale we apply at the beginning to ease the registrations

    Return value : -------------------------------------------------------

    Dictionnary that contains the warped images, static image padded, the mappings computed, and the rescale factor used at the beginning
    The keys are : "warped_images" / "static_image" / "mappings" / "rescale_factor"
    
    """
    # Pad the two images to the size of the biggest one
    # We pad both of the images because we don't know a priori which image is the big one.

    if rescale_image:
        if rescale_factor == None:
            rescale_factor = static.shape[0]/moving.shape[0]
            rescale_factor = round(rescale_factor,1)
        moving = transform.rescale(moving, rescale_factor)
    maxw, maxh = (np.max((moving.shape[0], static.shape[0])), np.max((moving.shape[1], static.shape[1])))

    moving_reframed = np.zeros((maxw, maxh))
    w, h = moving.shape
    moving_reframed = np.pad(moving, pad_width=((0, maxw-w), (0, maxh-h)), mode='constant')

    static_reframed = np.zeros((maxw, maxh))
    w, h = static.shape
    static_reframed = np.pad(static, pad_width=((0, maxw-w), (0, maxh-h)), mode='constant') 
    

    # Create an affine registration object
    ar = AffineRegistration()
    
    rigidIsoScaling_reg = None
    mapping_rigidIsoScaling= None
    if center_registration:
        ## First step : Align the two center of mass :

        center_reg, center_reg_matrix = dipy.align.center_of_mass(moving_reframed, static_reframed, np.eye(3), np.eye(3)) # I don't understand the 2 last parameters, I put something to make it work :)
        # center_reg is the image moved
        # center_reg_matrix is the transformation matrix (3*3)

        ## Second Step : Compute an RigidIsoScaling (Scaling + Rotation + Translation) transform :
        mapping_rigidIsoScaling = ar.optimize(static_reframed, center_reg, RigidIsoScalingTransform2D(), None) # Put  'ret_metric=True' in parameters if you want the function to return the value of the metric at the end
        rigidIsoScaling_reg = mapping_rigidIsoScaling.transform(center_reg, 'linear')
    else:
        
        ## Second Step : Compute an RigidIsoScaling (Scaling + Rotation + Translation) transform :
        mapping_rigidIsoScaling = ar.optimize(static_reframed, moving_reframed, RigidIsoScalingTransform2D(), None) # Put  'ret_metric=True' in parameters if you want the function to return the value of the metric at the end 
        # Apply the transform to the moving image
        rigidIsoScaling_reg = mapping_rigidIsoScaling.transform(moving_reframed, 'linear')

    ## Third step : Compute a translation transform to make things work better
    mapping_translation= ar.optimize(static_reframed, rigidIsoScaling_reg, TranslationTransform2D(), None)

    # Apply the transform to the moving image
    translation_reg = mapping_translation.transform(rigidIsoScaling_reg, 'linear')
    
    mapping_diffeo = None
    diffeo_reg = None
    if diffeo_registration:
        # Metric used :  SSD is Sum Square Difference of the two images
        metric = SSDMetric(2)

        # Smooth Registration to avoid aggressive displacement field on our images
        if smooth_registration:
            sdr = SmoothSymmetricDiffeomorphicRegistration(metric, smooth_sigma= smooth_sigma)
        else:
            sdr = SymmetricDiffeomorphicRegistration(metric)

        mapping_diffeo = sdr.optimize(static_reframed, translation_reg)
        diffeo_reg = mapping_diffeo.transform(translation_reg, 'linear')
            
    if disp_figure:
        # See Result   
        # Plot the two images and their superposition
        regtools.overlay_images(static_reframed, moving_reframed, 'Static', 'Overlay', 'Moving')
        if center_registration:
            regtools.overlay_images(static_reframed, center_reg, 'Static', 'Overlay','CenterOfMassAlignment')
        regtools.overlay_images(static_reframed, rigidIsoScaling_reg, 'Static', 'Overlay','RigidIsoScaling Transform')
        regtools.overlay_images(static_reframed, translation_reg, 'Static', 'Overlay','Translation transform')
        if diffeo_registration:
            regtools.overlay_images(static_reframed, diffeo_reg, 'Static', 'Overlay','Diffeomorphic transform')
    
    if not rescale_image:
        rescale_factor=None

    if center_registration:
        res = {"warped_images": (center_reg, rigidIsoScaling_reg, translation_reg, diffeo_reg), 
               "static_image": static_reframed, "mappings": (AffineMap(center_reg_matrix), mapping_rigidIsoScaling, mapping_translation, mapping_diffeo), "rescale_factor":rescale_factor}
    else:
        res = {"warped_images": (None, rigidIsoScaling_reg, translation_reg, diffeo_reg), 
               "static_image": static_reframed, "mappings": (None, mapping_rigidIsoScaling, mapping_translation, mapping_diffeo), "rescale_factor":rescale_factor}
        
    return res


def registration_3D(moving, 
                    static, 
                    rescale_2D= False,
                    rescale_3D = False,
                    rescale_factor = None,
                    center_registration= False,
                    diffeo_registration = False, 
                    disp_figure=False, 
                    metric_name = "SSD", 
                    smooth_registration= True, 
                    level_iters_diffeo = [10,10,5],
                    smooth_sigma = 10.):
    """ registration_3D(moving, static, disp_figure = False, diffeo_registration = False)
    
    Compute the dipy registration on 3D images (not mask)

    Parameters -------------------------------------------------------

    moving : the moving image - numpy 3D array (zstack, width, height)
    static : the static image - numpy 3D array (zstack, width, height)

    disp_figure : boolean : True if we want to see the figures

    rescale_2D          : boolean :  True we we want to rescale the moving image (but not in the z direction) at the beginning to ease the registrations 
    rescale_3D          : boolean :  True we we want to rescale the moving image at the beginning to ease the registrations 
    rescale_factor      : double : Value of the rescale we apply at the beginning to ease the registrations
    center_registration : boolean : True if we want to compute a center_of_mass registration (sometimes it works better without)
    diffeo_registration : boolean : True if we want to compute a symmetric diffeomorphic registration (sometimes it's not useful and it takes a lot of time)
    smooth_registration : boolean : True if we want our diffeomorphic registration to be smooth
    smooth_sigma        : double  : value of the smoothness of the transformation
    level_iters_diffeo  : list of double : "the number of iterations at each level of the Gaussian Pyramid 
                            (the length of the list defines the number of pyramid levels to be used)" according to dipy documentation
    metric : "SSD" or "CC" : Metric used to compute the diffeomorphism registration. SSD for Sum Square Difference and CC for Cross Correlation
    Return value : -------------------------------------------------------

    Dictionnary that contains the warped images, the moving image padded (before registration), static image padded, the mappings computed and the final image (registered)
    The keys are : "warped_images" /"moving_image"/ "static_image" / "mappings" / "final_image"
    
    """
    # First, if wanted, we rescale the moving to help the registration

    if rescale_2D and rescale_3D:
        raise ValueError("rescale2D and rescale3D can't be both True")
    if rescale_2D or rescale_3D:
        if rescale_factor is None:
            rescale_factor = static.shape[1]/moving.shape[1]
            rescale_factor = round(rescale_factor,1)
        
        if rescale_2D:
            moving = rescale(moving, rescale_factor, channel_axis=0)
        elif rescale_3D:
            moving = rescale(moving, rescale_factor)

    # Pad the two images to the size of the biggest one
    # We pad both of the images because we don't know a priori which image is the big one.
    maxz, maxw, maxh = (np.max((moving.shape[0], static.shape[0])), np.max((moving.shape[1], static.shape[1])), np.max((moving.shape[2], static.shape[2])))
    z,w,h = moving.shape

    if (maxz-z)%2 ==0:
        moving_reframed = np.pad(moving, pad_width=((int((maxz-z)/2),int((maxz-z)/2)), (0, maxw-w), (0, maxh-h)), mode='constant')
    else:
        moving_reframed = np.pad(moving, pad_width=((int((maxz-z)/2),int((maxz-z)/2) +1), (0, maxw-w), (0, maxh-h)), mode='constant')   
        
    z,w,h = static.shape
    if (maxz-z)%2 ==0:

        static_reframed = np.pad(static, pad_width=((int((maxz-z)/2),int((maxz-z)/2)),(0, maxw-w), (0, maxh-h)), mode='constant') 
    else:
        static_reframed = np.pad(static, pad_width=((int((maxz-z)/2),int((maxz-z)/2) +1),(0, maxw-w), (0, maxh-h)), mode='constant') 

    static = static_reframed
    moving = moving_reframed

    if disp_figure:
        regtools.overlay_slices(static, moving, None, 0,
                                "Static", "Before registration")
        regtools.overlay_slices(static, moving, None, 1,
                                "Static", "Before registration")
        regtools.overlay_slices(static, moving, None, 2,
                                "Static", "Before registration")

    c_of_mass = None
    c_of_mass_reg = None

    if center_registration:
        c_of_mass = transform_centers_of_mass(static, None,
                                      moving, None)
        c_of_mass_reg = c_of_mass.transform(moving)

        if disp_figure:
            regtools.overlay_slices(static, c_of_mass_reg, None, 0,
                                    "Static", "Transformed", )
            regtools.overlay_slices(static, c_of_mass_reg, None, 1,
                                    "Static", "Transformed")
            regtools.overlay_slices(static, c_of_mass_reg, None, 2,
                                    "Static", "Transformed")

    # We create our Affine Registration object
    nbins = 32
    sampling_prop = None
    metric = MutualInformationMetric(nbins, sampling_prop) # We use the Mutual information Metric
    level_iters = [10000, 1000, 100]
    factors = [4, 2, 1]
    sigmas = [3.0, 1.0, 0.0]

    affreg = AffineRegistration(metric=metric,
                                level_iters=level_iters,
                                sigmas=sigmas,
                                factors=factors)

    # We create our affine transform object
    transform = AffineTransform3D()
    if center_registration:

        affine = affreg.optimize(static, c_of_mass_reg, transform, None,
                                None, None,
                                starting_affine=None)
        
        transformed = affine.transform(c_of_mass_reg)

    else:
        affine = affreg.optimize(static, moving, transform, None,
                                None, None,
                                starting_affine=None)
        
        transformed = affine.transform(moving)

    if disp_figure:
        regtools.overlay_slices(static, transformed, None, 0,
                                "Static", "Affine_registration")
        regtools.overlay_slices(static, transformed, None, 1,
                                "Static", "Affine_registration")
        regtools.overlay_slices(static, transformed, None, 2,
                                "Static", "Affine_registration")

    mapping = None
    warped_moving = None
    if diffeo_registration:
            
        if metric_name == "SSD":
            metric = SSDMetric(3)
        elif metric_name == "CC":
            metric = CCMetric(3)
        else:
            raise ValueError(metric_name +" not known")

        level_iters = level_iters_diffeo
        if smooth_registration:
            sdr = SmoothSymmetricDiffeomorphicRegistration(metric=metric, level_iters=level_iters, smooth_sigma=smooth_sigma)
        else:
            sdr = SymmetricDiffeomorphicRegistration(metric=metric, level_iters=level_iters)

        mapping = sdr.optimize(static, transformed)
        warped_moving = mapping.transform(transformed)

        if disp_figure:
            regtools.overlay_slices(static, warped_moving, None, 0,
                                    "Static", "Diffeo_registration")
            regtools.overlay_slices(static, warped_moving, None, 1,
                                    "Static", "Diffeo_registration")
            regtools.overlay_slices(static, warped_moving, None, 2,
                                    "Static", "Diffeo_registration")

    res = {"warped_images":(c_of_mass_reg, transformed, warped_moving), "moving_image": moving, "static_image": static, "mappings":(c_of_mass, affine, mapping)}
    if diffeo_registration:
        res["final_image"] = warped_moving
    else:
        res["final_image"] = transformed

    return res


def apply_2Dtransform(dipy_transform, 
                    moving_img, 
                    static_img, 
                    disp_figure = False):
    """ apply_2Dtransform(dipy_transform, moving_img, static_img, disp_figure = False)

    Apply the computed transform on our images
    Apply the transform computed on our images :
    The function still works when we put bigger images than the ones used for the computation
    
    Parameters ---------------------------------------------------

    dipy_transform : dictionnay, it's the return of the dipy_registration function
    moving_img : moving image : numpy 2D array
    static_img : static image : numpy 2D array

    disp_figure : boolean : True if we want to see the figures

    Return value ----------------------------------------------------

    Dictionary that contains the static images padded, the final image and all the warped images
    keys : "static_image" / "final_image"/ "warped _images"
    """
    if dipy_transform["rescale_factor"] is not None:
        moving_img = transform.rescale(moving_img, dipy_transform["rescale_factor"])

    static= dipy_transform["static_image"] # Only to get the width and the height of the image
    tmp_w, tmp_h = (np.max((moving_img.shape[0], static_img.shape[0])), np.max((moving_img.shape[1], static_img.shape[1]))) #get the max width and height to compute the scale factor
    scale_factor = static.shape[0]/tmp_w
    
    mappings = dipy_transform["mappings"] # Get the mapping of the differents transforms
    
    # To know which transformation we computed 
    center_reg = (mappings[0] is not None) # True if you computed a center_of_mass registration
    diffeo_reg = (mappings[3] is not None) # True if you computed a symmetric_diffeomorphic registration
    
    # We inititialize the differents variables
    moving_image_reframed = None
    static_image_reframed = None
    center_img_reg = None
    rigidIsoScaling_img_reg = None
    translation_img_reg =  None
    diffeo_img_reg = None
    
    if scale_factor !=1: # If we scaled the images, we need to upscale the mappings
        width, height = tuple(int(dim/scale_factor) for dim in static.shape) # Get the size of our big images padded
        
        # Pad the images to get the good size
        w, h = moving_img.shape
        moving_image_reframed = np.zeros((width, height))
        moving_image_reframed = np.pad(moving_img, pad_width=((0, width-w), (0, height-h)), mode='constant')
        
        
        
        w, h = static_img.shape
        static_image_reframed = np.zeros((width, height))
        static_image_reframed = np.pad(static_img, pad_width=((0, width-w), (0, height-h)), mode='constant')
        
        if center_reg:
            # Apply the first transform (center of mass registration), we need to upscale the translation part of the matrix
            affinemap = mappings[0].get_affine()
            affinemap[0:2,2]= (int(1/scale_factor))*affinemap[0:2,2]
            mapping1 = AffineMap(affinemap) # create an affine map based on the new matrix

            # Apply the transform on the moving_image
            center_img_reg = mapping1.transform(moving_image_reframed, 'linear', sampling_grid_shape =(width, height))
        
        # Apply the second transform (RigidIsoScaling), we need to upscale the translation part of the matrix
        affinemap = mappings[1].get_affine()
        affinemap[0:2,2]= (int(1/scale_factor))*affinemap[0:2,2]
        mapping2 = AffineMap(affinemap)
        
        if center_reg:
            # Apply the matrix to the image registered with the previous transform        
            rigidIsoScaling_img_reg = mapping2.transform(center_img_reg, 'linear', sampling_grid_shape =(width, height))
        else:
            # Apply the matrix to the image registered with the previous transform        
            rigidIsoScaling_img_reg = mapping2.transform(moving_image_reframed, 'linear', sampling_grid_shape =(width, height))
            
        # Apply the third transform (translation transform), we need to upscale the translation part of the matrix
        affinemap = mappings[2].get_affine()
        affinemap[0:2,2]= (int(1/scale_factor))*affinemap[0:2,2]
        mapping3 = AffineMap(affinemap) # create an affine map based on the new matrix
        # Apply the transformation the image registered with the previous transformation
        translation_img_reg = mapping3.transform(rigidIsoScaling_img_reg, 'linear', sampling_grid_shape =(width, height))
        
        if diffeo_reg:
            # Apply the fourth transform (diffeomorphic transform), we need to upscale the vector field
            # Apply the transformation the image registered with the previous transformation

            # Create a new DiffeomorphicMap
            mapping4 = DiffeomorphicMap(2, (width, height))
            # Upscale the norm of the vectors
            mapping4.forward =  int(1/scale_factor)*(mappings[3].get_forward_field())
            mapping4.backward = int(1/scale_factor)*(mappings[3].get_backward_field())
            # Resample the field to the new size
            mapping4.expand_fields(np.array([scale_factor,scale_factor]), np.array([width,height], dtype=np.int32))
            # Warp the image with warp_2d nested function because otherwise it doesn't work
            diffeo_img_reg = vfu.warp_2d(translation_img_reg,mapping4.get_forward_field().astype(np.float64))            
           
    else: # If we didn't downscale the image, it's more simple
        
        maxw, maxh = static.shape[0], static.shape[1]
        # We pad the two image to make them the same size
        moving_image_reframed = np.zeros((maxw, maxh))
        w, h = moving_img.shape
        moving_image_reframed = np.pad(moving_img, pad_width=((0, maxw-w), (0, maxh-h)), mode='constant')

        static_image_reframed = np.zeros((maxw, maxh))
        w, h = static_img.shape
        static_image_reframed = np.pad(static_img, pad_width=((0, maxw-w), (0, maxh-h)), mode='constant') 
        
        # Apply the three transform in a row just like before, no need to upscale the matrix since it's the same size
        
        if center_reg:
            center_img_reg = mappings[0].transform(moving_image_reframed, 'linear', sampling_grid_shape =(maxw, maxh))
            rigidIsoScaling_img_reg = mappings[1].transform(center_img_reg, 'linear', sampling_grid_shape =(maxw, maxh))
        else:
            rigidIsoScaling_img_reg = mappings[1].transform(moving_image_reframed, 'linear', sampling_grid_shape =(maxw, maxh))
        translation_img_reg = mappings[2].transform(rigidIsoScaling_img_reg, 'linear', sampling_grid_shape =(maxw, maxh))

        if diffeo_reg:
           
            diffeo_img_reg = mappings[3].transform(translation_img_reg, 'linear')

    if disp_figure:
        # Visualize the three transforms
        regtools.overlay_images(static_image_reframed, moving_image_reframed, 'Static', 'Overlay',
                                'Before registration')
        if center_reg:   
            regtools.overlay_images(static_image_reframed, center_img_reg, 'Static', 'Overlay',
                                'Center reg')
        regtools.overlay_images(static_image_reframed, rigidIsoScaling_img_reg, 'Static', 'Overlay',
                                'Rigid Iso Scaling registration')

        regtools.overlay_images(static_image_reframed, translation_img_reg, 'Static', 'Overlay',
                                'Translation registration')
        if diffeo_reg:
            regtools.overlay_images(static_image_reframed, diffeo_img_reg, 'Static', 'Overlay',
                                'Diffeomorphic Registration ')
    
    # Return :
    # We return a dict that contains the static image, the final image and all the warped images
    if diffeo_reg:
        final_img = diffeo_img_reg
    else:
        final_img = translation_img_reg
    res = {"static_image": static_image_reframed, "warped_images" : (center_img_reg, rigidIsoScaling_img_reg, translation_img_reg, diffeo_img_reg), "final_image":final_img}
    return res




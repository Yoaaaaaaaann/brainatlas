from . import utils
from jsonpath_ng.ext import parse
import json
import lxml.etree as ET
from skimage import measure, transform
import re
import numpy as np

class Atlas:
        def __init__(self, 
                     atlas_fname=None, 
                     ontology_fname=None):
                """ Atlas
                Atlas class :
                An atlas object consist on an atlas image in .svg and an ontology file, hierarchically structured json file 
                that contains all the informations on the data.

                The json file and the svg image can both be found on Allen brain Atlas at:
                http://help.brain-map.org/display/api/Atlas+Drawings+and+Ontologies

                An atlas object has several attributes :
                atlas_image : the svg image
                atlas_fname : the filename of the image
                ontology : the json file with all the caracteristics of the brain
                atlas_areas : a dict that contains all the name and the id of the differents areas visible on the image.
                (because the ontology file gather many areas that are not visible on the atlas image : 
                our image is 2D but the ontology file works for the entire brain)
                """
                self.atlas_areas = None

                # Loading of the data
                self.atlas_fname = atlas_fname
                self.ontology_fname = ontology_fname

                if ontology_fname is not None:
                        if ".json" in ontology_fname:
                                self.ontology = json.load(open(ontology_fname,'r'))
                        else:
                                self.ontology =None
                                raise ValueError("Only .json files are supported yet")
                else:
                        self.ontology = None

                if atlas_fname is not None:
                        if ".svg" in atlas_fname:
                                self.atlas_image = open(atlas_fname).read()
                                
                        else:
                                self.atlas_image =None
                                raise TypeError("Only .svg files are supported yet")
                else:
                        self.atlas_image = None 


        def set_atlas_fname(self, 
                            atlas_fname):
                """ set_atlas_fname
                Set attribute : atlas_image and atlas_fname.
                atlas_fname has to be a svg file
                """
                if ".svg" in atlas_fname:
                        
                        self.atlas_fname = atlas_fname
                        self.atlas_image = open(atlas_fname).read()      
                        # We reset the atlas_areas because we change our atlas
                        self.atlas_areas = None     
                else:
                        raise TypeError("Only .svg files are supported yet")

        def set_ontology_fname(self, 
                               ontology_fname):
                """ set_ontology_fname
                Set attribute : ontology and ontology_fname.
                ontology has to be a json file
                """
                if ".json" in ontology_fname:
                        self.ontology_fname = ontology_fname   
                        self.ontology = json.load(open(ontology_fname,'r'))
                        # We reset the atlas_areas because we change our ontology
                        self.atlas_areas = None  
                else:
                        raise ValueError("Only .json files are supported yet")

        def set_atlas_image(self, 
                            atlas_image):
                """ set_atlas_image
                Directly set the attribute atlas_image
                atlas_image has to be an svg file
                """
                if not isinstance(atlas_image,str):
                        raise TypeError("The atlas is not a string")
                if "<svg" not in atlas_image:
                        raise ValueError("Atlas is not an svg file")
                
                self.atlas_image = atlas_image
                
        def set_ontology(self, 
                         ontology):
                """ set_ontology
                Directly set the attribute ontology
                ontology has to be a dictionnary.
                """
                if not isinstance(ontology ,dict):
                        raise TypeError("The ontology is not a dictionnary")

                self.ontology = ontology

                
        def get_id_by_name(self, 
                           roi):
                """ get_if_by_name
                This function returns the id of a given area name

                Precondition : id and name are unique in the file
                Parameters : string -> roi : containing the name of the region of interest
                Return :  int -> id of the roi
                """
                if self.ontology is not None:
                        jsonpath_expression = parse('$..children[?(name=="'+roi+'")].id')
                        id = jsonpath_expression.find(self.ontology)
                        if(id==[]):
                                raise ValueError("No matching id for this brain area")
                        id = id[0].value
                        return id
                else:
                        raise AttributeError("Attribute ontology can't be None")

        def get_name_by_id(self, 
                           id_roi):
                """ get_name_by_id
                This function returns the name of a given area id

                Precondition : id and name are unique in the file
                Parameters : id -> id_roi : containing the id of the region of interest
                Return :  string -> name of the roi
                """
                if self.ontology is not None:
                        jsonpath_expression = parse("$..children[?(id=="+str(id_roi)+")].name")
                        name = jsonpath_expression.find(self.ontology)
                        if(name==[]):
                                raise ValueError("No matching name for this brain area ID")
                        name = name[0].value
                        return name
                raise AttributeError("Attribute ontology can't be None")

        def get_all_children(self, 
                             roi):
                """ get_all_children
                This function returns a liste of ids of all the children of an given area name

                A children of an area is a subregion of this area

                Parameter : string -> roi : containing the name of the region of interest
                Return : list of int : ids :  id of all the children of this area
                """
                if self.ontology is not None:
                        jsonpath_expression = parse('$..children[?(name=="'+roi+'")]')
                        # We find the roi
                        names = jsonpath_expression.find(self.ontology)
                        if names==[]:
                                raise ValueError("This region of interest is not referenced")
                        names = str(names[0].value)      
                        # We find the position of all occurence of "id" in the roi
                        position_of_ids = [m.start() for m in re.finditer("'id':", names)]
                        # We find all the ids
                        ids = [int(names[pos+5:pos+30].split(',')[0]) for pos in position_of_ids]
                        return ids
                else:
                        raise AttributeError("Attribute ontology can't be None")
        
        def get_all_ontology_areas(self):
                """ get_all_ontology_areas
                This function returns  dict containing all the areas name and id that are referenced in the ontology file.
                """
                if self.ontology is not None:
                        jsonpath_expression = parse("$..children")
                        children = jsonpath_expression.find(self.ontology)      
                        areas={}
                        children = [child.value for child in children]
                        for childList in children:
                                for child in childList:
                                        areas[int(child["id"])] = child["name"]
                        return areas
                else:
                        raise AttributeError("Attribute ontology can't be None")

        
        def area_in_atlas(self, 
                          id_roi, 
                          path_id):
                """area_in_atlas(roi, path_id)
                This function returns a boolean that said if the region of interest is represented or not on the svg image.

                We look at all the roi children to know if one of the children match with a vector in the svg file.

                Parameters : int -> roi : id of the region of interest
                        list of int -> path_id : liste of all ids that represented by a vector in the svg image.
                
                Return : boolean : True if the area if represented, False if not
                """

                if id_roi in path_id:
                        return True

                jsonpath_expression = parse("$..children[?(id=="+str(id_roi)+")]")
                children = jsonpath_expression.find(self.ontology)
                if children!=[]:
                        children = str(children[0].value)
                else:
                        raise ValueError("id : " +str(id_roi)+ " is not known in the ontology file")
                position_of_ids = [m.start() for m in re.finditer("'id':", children)]
                for pos in position_of_ids:
                        if int(children[pos+5:pos+30].split(',')[0]) in path_id:
                                return True

                return False

        def get_all_atlas_areas(self):
                """ get_all_atlas_areas
                This functions returns a liste of id of all the areas that are represented on the atlas_image 
                including the "overarea" i.e. regions that englobe several vector of the svg file
                """
                if self.atlas_areas is not None:
                        return self.atlas_areas
                else:
                        if self.atlas_image is not None and self.ontology is not None:
                                path_id = self.get_all_svg_path()
                                areas = self.get_all_ontology_areas()
                                atlas_areas={}
                                for id, name in areas.items():
                                        if self.area_in_atlas(id,path_id):
                                                atlas_areas[id] = name                        
                                self.atlas_areas = atlas_areas
                                return atlas_areas
                        else:
                                raise AttributeError("Atlas and Ontology attributes can't be None")

        def get_all_svg_path(self):
                """ get_all_atlas_areas
                This functions returns a liste of id of all the vectors (i.e. <path> tag) of the atlas_image 
                """
                if self.atlas_image is not None and self.ontology is not None:
                        ids=set()
                        # We go through the svg file and we get the "structure_id"
                        tree = ET.fromstring(self.atlas_image)
                        for element in tree.iter():
                                if element.tag.split("}")[1] == "path":
                                        ids.add(int(element.get("structure_id")))
                        return ids
                else:
                        raise AttributeError("Atlas and Ontology attributes can't be None")


        def color_atlas(self, 
                        list_id, color, 
                        change_atlas=False):
                """ color_atlas
                This function colors the atlas_image.
                The vectors colored are all the vectors include in the liste_id liste.

                Parameters : list of int -> list_id :  list of ids of the structures that we want to color
                        tuple, list or array -> color : array represented the (R, G B) colors between 0 and 255.
                        boolean -> change_atlas : True if we want the attribute atlas_image to be changed to the colored image
                Return : string -> svg file with the atlas image modified
                """
                if self.atlas_image is not None:
                        # We create a ElementTree object from the atlas_image
                        tree = ET.fromstring(self.atlas_image)
                        # We browse the tree
                        for element in tree.iter():
                                #if the element is a <path> tag
                                if element.tag.split("}")[1] == "path":
                                        id = element.get("structure_id")
                                        if (int(id) in list_id):
                                                # We get the previous color
                                                save_color = element.get("style")
                                                # We save the previous color in a new attribute
                                                element.set("save_color", save_color)
                                                # We set the new color
                                                element.set("style","fill:rgb("+str(color[0])+","+str(color[1])+","+str(color[2])+");stroke-width:16;stroke:rgb("+str(color[0])+","+str(color[1])+","+str(color[2])+")")
                        if change_atlas:
                                self.atlas_image = ET.tostring(tree)

                        return ET.tostring(tree)
                else:
                        raise AttributeError("Atlas attribute can't be None")

        def reset_color(self):
                """ reset_color
                This function reset all the colors of the atlas to the original colors.
                """
                if self.atlas_image is not None:
                        tree = ET.fromstring(self.atlas_image)
                        for element in tree.iter():
                                if element.tag.split("}")[1] == "path":
                                        if (element.get("save_color") is not None):
                                                element.set("style", element.get("save_color"))
                        self.atlas_image = ET.tostring(tree)
                else:
                        raise AttributeError("Atlas attribute can't be None")

        def get_contour_area(self, 
                             roi, 
                             scale_factor=1, 
                             color=None, 
                             change_atlas=False):
                """ get_contour_area
                This is the main function of the module.
                It returns all the contours and the mask of the specified area.

                Parameters : string-> roi : name of the region of interest
                             number -> scale_factor : we can upscale or downscale the contour and the mask if we work on smaller images.
                             tuple, list or array -> color : array represented the (R, G B) colors between 0 and 255.
                             boolean-> change_atlas :  True if we want to change the atlas_image to an image where the region is colored
                Return : a list of 2d array that represent our contour
                         the mask of the region of interest
                """
                if self.atlas_areas is not None:
                        if roi not in self.atlas_areas.values():
                                raise ValueError("Area not represented")
                # get all the children of the area
                liste_id=self.get_all_children(roi)

                # add the id of the area itself
                id_roi = self.get_id_by_name(roi)
                liste_id.append(id_roi)
                
                if color is None:
                        color = (153,0,255) #list(np.random.choice(range(256), size=3))

                # Color the atlas
                svg_img = self.color_atlas(liste_id, color, change_atlas=change_atlas)

                # Convert the svg image into a numpy array
                img_array = utils.svg_to_ndarray(svg_img)
                # The shape is (w,h,4), the fourth axis is a mask of the brain.
                # We keep the 3 first axis (the 3 colors)
                img_array = img_array[...,:-1]

                # We create a mask by keeping the pixel that have the exact same color as the chosen color
                img_mask = (img_array[:,:,0]==color[0]) & (img_array[:,:,1] ==color[1]) & (img_array[:,:,2]==color[2])

                # We rescale the image if necessary
                if scale_factor != 1:
                        img_mask = utils.pad_image(img_mask, scale_factor)
                        img_mask = transform.rescale(img_mask, scale_factor)
                
                # We find the contour of the 0.999999 value.
                contours = measure.find_contours(img_mask, 0.999999999)
                return contours, img_mask



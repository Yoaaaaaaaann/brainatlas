from scipy.interpolate import interp1d
from scipy.signal import find_peaks
import skimage
import skimage.transform
import skimage.util
import skimage.exposure
import numpy as np

# Make masks of hemispheres
def compute_threshold(img, 
                      cut=0.33, 
                      step=15):
    """ compute_threshold(name,img, step=15)
    
    Compute the threshold to get the best mask possible :
    The function find the two firsts peeks of the image's histogram and then return the value between them.
    
    Parameters --------------------------------------
    
    img : image we want to compute the threshold for the mask : numpy 2D array
    cut : factor that may be interesting to play width : it's where we cut between the 2 firsts peeks :         
            cut = 0.25 # Peek1-----|---------------Peek2
        
            cut = 0.3  # Peek1-------|-------------Peek2
        
            cut = 0.6  # Peek1------------|--------Peek2

            cut parameter can be <0 or >1, it still works
    step : the step used to create the linespace when we interpolate
    """
    
    hist, hist_centers = skimage.exposure.histogram(img)
    #plt.plot(hist_centers, hist)
    
    # Cut off leftmost and rightmost peaks
    left, right = 1, -1
    chist = hist[left:right]
    chist_centers = hist_centers[left:right]

    # Interpolate to smooth
    f = interp1d(chist_centers, chist, kind='cubic')
    xx = np.linspace(chist_centers[0], chist_centers[-1], 3*step)
    yy = f(xx)
    #plt.plot(xx, yy)
    #plt.show()
    peaks, peak_props = find_peaks(yy, distance=10)
    
    if yy[peaks][0] >200: #>200 to avoid very small peeks -> may be improved
        return xx[peaks][0] + cut * (xx[peaks][1] - xx[peaks][0])
    else:
        return xx[peaks][1] + cut * (xx[peaks][2] - xx[peaks][1])

def make_mask(img, 
              cut=0.33, 
              step=15):
    """ make_mask
    
    Create a mask : True if the value is greater than the threshold, False otherwise
    
    The threshold is compute as :
    
    The function find the two firsts peeks of the image's histogram and then return the value between them.
    
    Parameters --------------------------------------
    
    img : image we want to compute the threshold for the mask : numpy 2D array
    cut : factor that may be interesting to play width : it's where we cut between the 2 firsts peeks :         
            cut = 0.25 # Peek1-----|---------------Peek2
        
            cut = 0.3  # Peek1-------|-------------Peek2
        
            cut = 0.6  # Peek1------------|--------Peek2
    
    step : the step used to create the linespace when we interpolate
    """
    mask = (img >compute_threshold(img, cut, step))
    return mask


import numpy as np
import matplotlib.pyplot as plt
from cairosvg import svg2png
from base64 import b64encode
from IPython.display import HTML, display
from imageio.v2 import imread

def plot_field(field):
    """ plot_field(field)
    
    Util function : plot the displacement field for the diffeomorphic_registration
    
    """
    shape = field.shape
    for i in range(shape[0]):
        for j in range(shape[1]):  
            if i%int(shape[0]/20)==0 and j%int(shape[1]/20)==0:
                plt.quiver(i,j,field[i,j,0],field[i,j,1])
    plt.show()

def plot_images(imgs, same_size= True):
    """ plot images

    Parameters : imgs
    works when :
    imgs is [imgs1, imgs2, imgs3]
    imgs is [[imgs1, imgs2],[imgs3, imgs4]]
    imgs is a dictionary
    We have to say if the images have the same size or not with "same_size" parameter
    /!\ Baddly coded, may not work well , it was just to avoid copy-paste pyplot function
    """

    keys =[]
    if isinstance(imgs, dict):
        keys = np.array(list(imgs.keys()))
        if same_size:
            imgs = np.array(list(imgs.values()))
        else:
            imgs = np.array(list(imgs.values()), dtype=np.ndarray)
    else:
        if same_size:
            imgs = np.array(list(imgs))
        else:
            imgs = np.array(list(imgs), dtype=np.ndarray)

    if len(imgs) ==1:
        plt.imshow(imgs[0])
        plt.show()
    else: 

        if len(imgs.shape) == 1 or imgs.shape[1]>10: 
            c =imgs.shape[0]
            fig, axs = plt.subplots(1, c, figsize=(16, 10))
            for i in range(c):
                axs[i].imshow(imgs[i])
                if len(keys)>0:
                    axs[i].set_title(str(keys[i]))
        else:
            l,c = imgs.shape 
            fig, axs = plt.subplots(l, c, figsize=(16, 10))

            for i in range(l):
                for j in range(c):
                    axs[i,j].imshow(imgs[i][j])


def svg_to_ndarray(svg):

        svg2png(bytestring=svg, write_to='tmp.png')
        return(imread('tmp.png'))


def plot_svg(svg):
    if not isinstance(svg, bytes):
        svg = str.encode(svg)
    encoded_svg = b64encode(svg)
    decoded_svg = encoded_svg.decode('ascii')
    st = r'<img class="figure" src="data:image/svg+xml;base64,{}" width={}% height={}%></img>'.format(decoded_svg, 50, 50)
    display(HTML(st))

def pad_image(img, scale_factor):
    
    w,h = img.shape
    new_w, new_h = img.shape
    if w%int(1/scale_factor) !=0:
        new_w = int(int(w*scale_factor)/scale_factor)+int(1/scale_factor)

    if h%int(1/scale_factor)!=0:       
        new_h = int(int(h*scale_factor)/scale_factor)+int(1/scale_factor)

    img_padded = np.zeros((new_w, new_h))
    img_padded = np.pad(img, pad_width=((0, new_w-w), (0, new_h-h)), mode='constant')
    return img_padded
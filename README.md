# Brainatlas

Brainatlas is a python library made to help researchers register their images onto an atlas.
It has been created to work with Mouse brain 2D or 3D images.

In this repository, you can find the python librairy (in which there is a readme file to learn how to install it)
and several jupyter notebook to show you how to use the library in more detailed examples
